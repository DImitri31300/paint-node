let col = 0;

function setup() {
    createCanvas(800, 700); // taille de la zone
  }
  
  function draw() {

    if (mouseIsPressed) {
      stroke(col);
      strokeWeight(20);
      line(mouseX, mouseY, pmouseX, pmouseY);
    }
  
    noStroke();
    fill(220,20,60);
    rect(0, 0, 100, 100);
  
    fill(154,205,50);
    rect(100, 0, 100, 100);
  
    fill(30,144,255);
    rect(200, 0, 100, 100);

    fill(255, 204, 0);
    rect(300, 0, 100, 100);

    fill(0);
    rect(400, 0, 100, 100);

    fill(255, 255, 255);
    rect(500, 0, 100, 100);

    fill(221,160,221);
    rect(600, 0, 100, 100);

    fill(220);
    rect(700, 0, 100, 100);
  
  }

function mousePressed() {
    if (mouseY < 100) {
      col = get(mouseX, mouseY);
    }
  }

