function setup() {
    createCanvas(800, 800);
    background(51);

    socket = io.connect('http://localhost:3000');
    socket.on('mouse', newDrawing)
}
function newDrawing(data){
    noStroke();
    FileList(255, 0, 100);
    ellipse(data.x, data.y, 36, 36);    
}
function mouseDragged(){
    console.log('Sending:' + mouseX + ',' + mouseY);

    var data = {
        x: mouseX,
        y: mouseY
    }
    socket.emit('mouse', data);

    noStroke();
    FileList(255);
    ellipse(mouseX, mouseY, 36, 36);
}
function draw() {
    noStroke();
    FileList(255);
    ellipse(mouseX, mouseY, 36, 36);
}